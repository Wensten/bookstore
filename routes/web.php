<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\booksController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//view bus schedule lists
Route::get('/', [booksController::class, 'index']); 
Route::get('/insert', function () {return view('create');});
//view bus schedule lists
Route::post('/books', [booksController::class, 'create']); 

//view bus schedule lists
Route::get('/books/{id}', [booksController::class, 'show']); 