<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Books;

class booksController extends Controller
{
    public function index()
    {
        $data = Books::all();
        return view('index',compact('data'));
    }

    public function create(Request $request)
    {
        $books = new Books();
        $books->create($request->all());
    }

    public function show($id)
    {
        $details = Books::where('id',$id)->get();
        return view('index',compact('data'));
    }

}
