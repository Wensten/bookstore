@extends('layout')
@section('content')

<div class="row">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style>
    .homebtn {
      margin-top: 12px;
      margin-left: 30px;
      background-color: #6bb5ff;
      border: none;
      color: black;
      float: left;
      padding: 14px 18px;
      font-size: 16px;
      cursor: pointer;
      border-radius: 5px;
    }

    /* Darker background on mouse-over */
    .homebtn:hover {
      background-color: #085099;
      color: white;
    }
    .btn-btn-primary {
      border: 20px DodgerBlue;
      background-color: #6bb5ff;
      color: black;
      padding: 14px 28px;
      font-size: 18px;
      cursor: pointer;
      float:right;
      border-radius: 5px;
    }
    .btn-btn-primary:hover {
      background-color: #085099;
      color: white;
      text-decoration: none;
    }
  </style> 
</div>

<div class="row">
  <a style="margin: 15px;"  href="http://127.0.0.1:8000/insert" class="btn-btn-primary">Add New Book</a>
</div> 

    <div class="col-sm-12">
        <h1 class="display-3">Books</h1>    
        <table class="table table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Book Name</th>
                <th>Author Name</th>
                <th>ISBN Number</th>
                <th>Edition</th>
                <th>Published Year</th>
                <th>Quantity</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
        @foreach($data as $a)
            <tr>
                <td>{{$a->id}}</td>
                <td>{{$a->name}}</td>
                <td>{{$a->author}}</td>
                <td>{{$a->ISBN}}</td>            
                <td>{{$a->edition}}</td>            
                <td>{{$a->p_year}}</td>            
                <td>{{$a->quantity}}</td>            
                <td>{{$a->price}}</td>            
            </tr>
        @endforeach
        <tbody>
        </table> 
    </div>      
@endsection