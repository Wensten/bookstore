

<div class="row">
  <style>
     
    .btn-btn-primary , .btn-btn-primary-outline {
      border: 20px DodgerBlue;
      background-color: #6bb5ff;
      color: black;
      padding: 14px 28px;
      font-size: 18px;
      cursor: pointer;
      float:right;
      border-radius: 5px;
    }

    .btn-btn-primary-outline{
      float:left;
    }

    .btn-btn-primary:hover , .btn-btn-primary-outline:hover{
      background-color: #085099;
      color: black;
      text-decoration: none;
    }

    .fill{
      padding-left: 40px;
      bottom-width:30px;
    }

    .spaces{
        padding-top:10px;
        padding-bottom:10px;
    }

    .spaces.blank{
        left-width: 10%;
    }

    .price{
        bottom: 20px;
    }


  </style>

</div>

<div class="row">
  <a style="margin: 15px;"  href="http://127.0.0.1:8000/" class="btn-btn-primary">Back to Book List</a>
</div> 


<div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a Book</h1>
    <div>
      <form action="http://127.0.0.1:8000/books" class="fill" method='post'>  
            <div class="spaces">
            <label for="name">Book Name:</label>
            <input type="text" class="blank" name="name" required/><br>
            </div>  
            <div class="spaces">
            <label for="author">Author Name:</label>
            <input type="text" class="blank" name="author" required/><br>
            </div> 
            <div class="spaces">
            <label for="ISBN">ISBN Number:</label>
            <input type="text" class="blank" name="ISBN" required/><br>
            </div> 
            <div class="spaces">
            <label for="edition">Edition:</label>
            <input type="text" class="blank" name="edition" required/><br>
            </div>
            <div class="spaces">
            <label for="p_year">Published Year:</label>
            <input type="date" class="blank" name="p_year" required/><br>
            </div> 
            <div class="spaces">
            <label for="quantity">Quantity:</label>
            <input type="number" class="blank" name="quantity" required/><br>
            </div> 
            <div class="spaces">
            <label for="price">Price:</label>
            <input type="text" class="price" name="price" required/><br>
            </div>                  
          <button type="submit" class="btn-btn-primary-outline">Add Book</button>
      </form>
    </div>
  </div>
